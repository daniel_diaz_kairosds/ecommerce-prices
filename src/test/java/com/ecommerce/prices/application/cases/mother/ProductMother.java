package com.ecommerce.prices.application.cases.mother;

import com.ecommerce.prices.domain.model.Product;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ProductMother {

    public static Product getProduct1() {
        return new Product(35455, 1,
                LocalDateTime.parse("2020-06-14T00:00:00", DateTimeFormatter.ISO_DATE_TIME),
                LocalDateTime.parse("2020-12-31T23:59:59", DateTimeFormatter.ISO_DATE_TIME),
                1, BigDecimal.valueOf(35.50), "EUR", 0);
    }

    public static Product getProduct2() {
        return new Product(35455, 1,
                LocalDateTime.parse("2020-06-14T15:00:00", DateTimeFormatter.ISO_DATE_TIME),
                LocalDateTime.parse("2020-06-14T18:30:00", DateTimeFormatter.ISO_DATE_TIME),
                2, BigDecimal.valueOf(25.45), "EUR", 1);
    }

    public static Product getProduct3() {
        return new Product(35455,
                1,
                LocalDateTime.parse("2020-06-15T00:00:00", DateTimeFormatter.ISO_DATE_TIME),
                LocalDateTime.parse("2020-06-15T11:00:00", DateTimeFormatter.ISO_DATE_TIME),
                3,
                BigDecimal.valueOf(30.50),
                "EUR",
                1);
    }
}
