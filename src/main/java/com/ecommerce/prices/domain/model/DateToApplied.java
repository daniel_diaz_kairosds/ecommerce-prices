package com.ecommerce.prices.domain.model;

import java.time.LocalDateTime;
import java.util.Objects;

public record DateToApplied(LocalDateTime value) {
    public DateToApplied {
        if(Objects.isNull(value)){
            throw new IllegalArgumentException("DateToApplied cannot be null");
        }
    }
}
