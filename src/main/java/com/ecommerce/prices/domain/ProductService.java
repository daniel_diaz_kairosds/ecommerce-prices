package com.ecommerce.prices.domain;

import com.ecommerce.prices.domain.model.BrandId;
import com.ecommerce.prices.domain.model.DateToApplied;
import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.domain.model.ProductId;

import java.util.List;

public interface ProductService {

    List<Product> getProductListByDate(final BrandId brandId, final ProductId productId, final DateToApplied dateToApplied);

    Product filterProductByMaxPriority(List<Product> products);
}
