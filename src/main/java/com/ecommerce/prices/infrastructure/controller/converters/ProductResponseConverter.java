package com.ecommerce.prices.infrastructure.controller.converters;

import com.ecommerce.prices.infrastructure.controller.dto.ProductResponse;
import com.ecommerce.prices.domain.model.Product;
import org.mapstruct.Mapper;

@Mapper (componentModel = "spring")
public interface ProductResponseConverter {
    ProductResponse convert(Product input);
}
