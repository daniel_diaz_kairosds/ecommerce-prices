package com.ecommerce.prices.application.services;

import com.ecommerce.prices.domain.ProductRepository;
import com.ecommerce.prices.domain.ProductService;
import com.ecommerce.prices.domain.exceptions.PriceNotFoundException;
import com.ecommerce.prices.domain.model.BrandId;
import com.ecommerce.prices.domain.model.DateToApplied;
import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.domain.model.ProductId;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class ProductApplicationService implements ProductService {

    private final ProductRepository productRepository;

    public ProductApplicationService(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getProductListByDate(final BrandId brandId, final ProductId productId, final DateToApplied dateToApplied) {
        return productRepository.searchProductsByDate(brandId.value(), productId.value(), dateToApplied.value());
    }

    @Override
    public Product filterProductByMaxPriority(final List<Product> products) {
        return CollectionUtils.emptyIfNull(products).stream()
                .sorted(Comparator.comparing(Product::price))
                .max(Comparator.comparing(Product::priority))
                .orElseThrow(() -> new PriceNotFoundException("There are no products with the indicated parameters"));
    }
}
