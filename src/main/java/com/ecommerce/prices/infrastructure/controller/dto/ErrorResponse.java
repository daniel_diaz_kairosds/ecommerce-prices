package com.ecommerce.prices.infrastructure.controller.dto;

import org.springframework.http.HttpStatus;

public record ErrorResponse(
    HttpStatus status,
    String message){
}
