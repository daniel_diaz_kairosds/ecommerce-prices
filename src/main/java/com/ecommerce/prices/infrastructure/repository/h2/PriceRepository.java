package com.ecommerce.prices.infrastructure.repository.h2;


import com.ecommerce.prices.domain.ProductRepository;
import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.infrastructure.repository.h2.converters.ProductConverter;
import com.ecommerce.prices.infrastructure.repository.h2.entities.Price;
import com.ecommerce.prices.infrastructure.repository.h2.entities.PriceRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class PriceRepository implements ProductRepository {

    private final ProductConverter converter;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String PRICE_QUERY =
            """
                SELECT
                    p.PRICE_LIST,
                    p.PRODUCT_ID,
                    p.BRAND_ID,
                    p.START_DATE,
                    p.END_DATE,
                    p.PRICE,
                    p.CURRENCY,
                    p.PRIORITY
                FROM PRICES p
                WHERE p.BRAND_ID = :brandId
                  AND p.PRODUCT_ID = :productId
                  AND p.START_DATE <= :dateToApplied
                  AND p.END_DATE >= :dateToApplied;
            """;

    public PriceRepository(final ProductConverter converter, final NamedParameterJdbcTemplate jdbcTemplate) {
        this.converter = converter;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Product> searchProductsByDate(final Integer brandId, final Integer productId, final LocalDateTime dateToApplied) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("brandId", brandId);
        params.addValue("productId", productId);
        params.addValue("dateToApplied", dateToApplied);

        final List<Price> prices = jdbcTemplate.query(PRICE_QUERY, params, new PriceRowMapper());
        return prices.stream().map(converter::convert).toList();
    }
}
