package com.ecommerce.prices;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class EcommercePricesApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @ParameterizedTest
    @CsvFileSource(resources = "/test-data.csv", numLinesToSkip = 1)
    void shouldReturnACorrectProducts_WhenProductsExistForDataTest(final String requestDate, final String requestBrandId, final String requestProductId,
                                                       final String expectedFare, final String expectedStartDate, final String expectedEndDate, final String expectedPrice) {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/api/v1/brands/")
                        .path(requestBrandId)
                        .path("/products/")
                        .path(requestProductId)
                        .queryParam("dateToApplied", requestDate)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.productId").isEqualTo(requestProductId)
                .jsonPath("$.brandId").isEqualTo(requestBrandId)
                .jsonPath("$.fare").isEqualTo(expectedFare)
                .jsonPath("$.startDate").isEqualTo(expectedStartDate)
                .jsonPath("$.endDate").isEqualTo(expectedEndDate)
                .jsonPath("$.price").isEqualTo(expectedPrice)
                .jsonPath("$.currency").isEqualTo("EUR");
    }

    @Test
    void shouldReturnNoFound_WhenProductNotExistTest() {
        final String requestBrandId = "2";
        final String requestProductId = "2343";
        final String dateToAppliedRequest = "2020-06-14T10:00:00";
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/api/v1/brands/")
                        .path(requestBrandId)
                        .path("/products/")
                        .path(requestProductId)
                        .queryParam("dateToApplied", dateToAppliedRequest)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void shouldReturnBadRequest_WhenDateToAppliedIsNotSent() {
        final String requestBrandId = "2";
        final String requestProductId = "2343";
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/api/v1/brands/")
                        .path(requestBrandId)
                        .path("/products/")
                        .path(requestProductId)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest();
    }
}
