-- auto-generated definition
create table PRICES
(
    BRAND_ID       INTEGER                not null,
    START_DATE     TIMESTAMP              not null,
    END_DATE       TIMESTAMP              not null,
    PRICE_LIST     INTEGER                not null,
    PRODUCT_ID     INTEGER                not null,
    PRIORITY       INTEGER                not null,
    PRICE          DOUBLE PRECISION       not null,
    CURRENCY       CHARACTER LARGE OBJECT not null,
    LAST_UPDATE    TIMESTAMP              not null,
    LAST_UPDATE_BY CHARACTER LARGE OBJECT not null,
    constraint PRICES_PK
        primary key (PRICE_LIST, BRAND_ID, PRODUCT_ID)
);

comment on table PRICES is 'Stores the final price per product and rate to be applied according to period and priority.';

comment on column PRICES.BRAND_ID is 'Group string ID';

comment on column PRICES.START_DATE is 'Date range in which the price applies';

comment on column PRICES.END_DATE is 'Date range in which the price applies';

comment on column PRICES.PRICE_LIST is 'Applicable price rate identifier.';

comment on column PRICES.PRODUCT_ID is 'Product code identifier.';

comment on column PRICES.PRIORITY is 'Price application disambiguate. If two rates coincide in a range of dates, the one with the higher priority (higher numerical value) is applied.';

comment on column PRICES.PRICE is 'Final sale price.';

comment on column PRICES.CURRENCY is 'ISO of the currency';

comment on column PRICES.LAST_UPDATE is 'Date of last update';

comment on column PRICES.LAST_UPDATE_BY is 'User who made the last update.';

create unique index PRICES_PRICE_LIST_UINDEX
    on PRICES (PRICE_LIST);