package com.ecommerce.prices.infrastructure.repository.h2.entities;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class PriceRowMapper implements RowMapper<Price> {
    @Override
    public Price mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Price(
                rs.getInt("PRICE_LIST"),
                rs.getInt("PRODUCT_ID"),
                rs.getInt("BRAND_ID"),
                rs.getObject("START_DATE", LocalDateTime.class),
                rs.getObject("END_DATE", LocalDateTime.class),
                rs.getBigDecimal("PRICE"),
                rs.getString("CURRENCY"),
                rs.getInt("PRIORITY")
        );
    }
}