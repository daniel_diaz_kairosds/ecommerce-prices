package com.ecommerce.prices.domain;

import com.ecommerce.prices.domain.model.Product;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductRepository {
    List<Product> searchProductsByDate(Integer brandId, Integer productId, LocalDateTime dateToApplied);
}
