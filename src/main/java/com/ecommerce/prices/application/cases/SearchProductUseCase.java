package com.ecommerce.prices.application.cases;

import com.ecommerce.prices.domain.ProductService;
import com.ecommerce.prices.domain.model.BrandId;
import com.ecommerce.prices.domain.model.DateToApplied;
import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.domain.model.ProductId;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class SearchProductUseCase {

    private final ProductService productsService;

    public SearchProductUseCase(final ProductService productsService) {
        this.productsService = productsService;
    }

    public Product searchProductByDateAndPriority(final Integer brandId, final Integer productId, final LocalDateTime dateToApplied) {
        final List<Product> products = productsService.getProductListByDate(new BrandId(brandId), new ProductId(productId), new DateToApplied(dateToApplied));
        return productsService.filterProductByMaxPriority(products);
    }

}
