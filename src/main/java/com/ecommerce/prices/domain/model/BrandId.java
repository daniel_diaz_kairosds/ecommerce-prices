package com.ecommerce.prices.domain.model;

import java.util.Objects;

public record BrandId (Integer value) {
    public BrandId {
        if(Objects.isNull(value)){
            throw new IllegalArgumentException("BrandId cannot be null");
        }
    }
}
