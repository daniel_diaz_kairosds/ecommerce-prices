# Ecommerce-prices Services

## Description

This project is a simple **microservice** in **Spring Boot** based on the **Clean Architecture** pattern and **API
Rest** service to query the fare and the price that applies to a product of a commercial brand in a specific date and
time.

## Technologies

* Java 17
* Spring Boot 3.3.4
* H2 Database 2.2.224
* Lombok 1.18.34
* MapsStruct 1.6.0

## Installation

To install and run the application, use maven commands from the root of the project:

To build and package:

```
mvn clean package spring-boot:repackage 
```

To run:

```
java -jar ./target/ecommerce-prices-0.0.1-SNAPSHOT.jar 
```

The application runs in localhost with 8080 port.

```
http://localhost:8080/api
```

## Price query service

Contains API related to query the fare of a product in a specific day it's a necessary pass:

* **brandId**: commercial chain identifier.
* **productId**: product id identifier.
* **dateToApplied**: the date and time to be applied.

| Method | Path                                                                        | Description                                                      | Scope  | Privilege  |
|--------|-----------------------------------------------------------------------------|------------------------------------------------------------------|--------|------------|
| GET    | /api/v1/brands/{brandId}/products/{productId}?dateToApplied={dateToApplied} | Get price and fare of a product of a commercial chain in a date. | server | ALL_ACCESS |

Http Status Code:

| HTTP status code | Description           |
|------------------|-----------------------|
| 200              | SUCCESS               |
| 404              | NOT FOUND             |
| 500              | INTERNAL SERVER ERROR |

The service retrieves a json with the following information of the product related to:

* **productId**: product id identifier.
* **brandId**: commercial chain identifier.
* **fare**: fare that applies to product.
* **startDate**: Starting date of applies of fare.
* **endDate**: End date of applies of fare.
* **price**: price of the product.
* **currency**: Currency in ISO format.

Examples.

Request:

```
http://localhost:8080/api/v1/brands/1/products/35455?dateToApplied=2020-06-14T16:00:00
```

Response:

```json
{
  "productId": 35455,
  "brandId": 1,
  "fare": 1,
  "startDate": "2020-06-14T00:00:00",
  "endDate": "2020-12-31T23:59:59",
  "price": 35.5,
  "currency": "EUR"
}
```
