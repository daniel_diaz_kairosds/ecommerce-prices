package com.ecommerce.prices.application.cases;

import com.ecommerce.prices.application.cases.mother.ProductMother;
import com.ecommerce.prices.domain.ProductRepository;
import com.ecommerce.prices.domain.exceptions.PriceNotFoundException;
import com.ecommerce.prices.domain.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@ActiveProfiles("test")
class SearchProductUseCaseTest {

    @MockBean
    private ProductRepository productRepository;

    @Autowired
    private SearchProductUseCase searchProductUseCase;

    @Test
    void giveNoExistingProduct_whenSearchProduct_shouldThrowException() {

        final Integer brandId = 1;
        final Integer productId = 35455;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T10:00:00", DateTimeFormatter.ISO_DATE_TIME);

        when(productRepository.searchProductsByDate(any(), any(), any())).thenReturn(null);

        final String errorMessage = assertThrows(PriceNotFoundException.class, () -> searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date)).getMessage();

        assertEquals("There are no products with the indicated parameters", errorMessage);
    }

    @Test
    void givenExistenProduct_whenSearchProduct_thenReturnProduct() {

        final Integer brandId = 1;
        final Integer productId = 35455;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T10:00:00", DateTimeFormatter.ISO_DATE_TIME);
        final Product expected = ProductMother.getProduct1();

        when(productRepository.searchProductsByDate(any(), any(), any())).thenReturn(List.of(expected));

        final Product result = searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date);

        assertEquals(expected, result);
    }

    @Test
    void givenMoreThanOneProductDifferentPriorities_whenSearchProduct_thenReturnProductWithMaxPriority() {

        final Integer brandId = 1;
        final Integer productId = 35455;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T16:00:00", DateTimeFormatter.ISO_DATE_TIME);

        final Product product1 = ProductMother.getProduct1();
        final Product product2 = ProductMother.getProduct2();

        when(productRepository.searchProductsByDate(any(), any(), any())).thenReturn(List.of(product1, product2));
        final Product result = searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date);

        assertEquals(product2, result);
    }

    @Test
    void givenMoreThanOneProductWithSamePriority_whenSearchProduct_thenReturnProductWithLowerPrice() {

        final Integer brandId = 1;
        final Integer productId = 35455;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T16:00:00", DateTimeFormatter.ISO_DATE_TIME);

        final Product product3 = ProductMother.getProduct3();
        final Product product2 = ProductMother.getProduct2();

        when(productRepository.searchProductsByDate(any(), any(), any())).thenReturn(List.of(product3, product2));
        final Product result = searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date);

        assertEquals(product2, result);
    }

    @Test
    void givenANullBrandId_whenSearchProduct_thenReturnException() {

        final Integer brandId = null;
        final Integer productId = 35455;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T16:00:00", DateTimeFormatter.ISO_DATE_TIME);

        final String errorMessage = assertThrows(IllegalArgumentException.class, () -> searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date)).getMessage();

        assertEquals("BrandId cannot be null", errorMessage);
    }

    @Test
    void givenANullProductId_whenSearchProduct_thenReturnException() {

        final Integer brandId = 1;
        final Integer productId = null;
        final LocalDateTime date = LocalDateTime.parse("2020-06-14T16:00:00", DateTimeFormatter.ISO_DATE_TIME);

        final String errorMessage = assertThrows(IllegalArgumentException.class, () -> searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date)).getMessage();

        assertEquals("ProductId cannot be null", errorMessage);
    }
    @Test
    void givenANullDateToApplied_whenSearchProduct_thenReturnException() {

        final Integer brandId = 1;
        final Integer productId = 35455;
        final LocalDateTime date = null;

        final String errorMessage = assertThrows(IllegalArgumentException.class, () -> searchProductUseCase.searchProductByDateAndPriority(brandId, productId, date)).getMessage();

        assertEquals("DateToApplied cannot be null", errorMessage);
    }
}