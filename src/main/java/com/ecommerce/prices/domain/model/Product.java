package com.ecommerce.prices.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;


public record Product(
        Integer productId,
        Integer brandId,
        LocalDateTime startDate,
        LocalDateTime endDate,
        Integer fare,
        BigDecimal price,
        String currency,
        Integer priority) {
}
