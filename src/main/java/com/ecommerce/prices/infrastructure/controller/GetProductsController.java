package com.ecommerce.prices.infrastructure.controller;

import com.ecommerce.prices.application.cases.SearchProductUseCase;
import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.infrastructure.controller.converters.ProductResponseConverter;
import com.ecommerce.prices.infrastructure.controller.dto.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/v1")
public class GetProductsController {

    private final SearchProductUseCase searchProductUseCase;
    private final ProductResponseConverter converter;

    @Autowired
    public GetProductsController(final SearchProductUseCase searchProductUseCase, final ProductResponseConverter converter) {
        this.searchProductUseCase = searchProductUseCase;
        this.converter = converter;
    }

    @GetMapping("/brands/{brandId}/products/{productId}")
    public ResponseEntity<ProductResponse> searchProductsByDate(@PathVariable Integer brandId, @PathVariable Integer productId,
                                                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateToApplied) {

        final Product product = searchProductUseCase.searchProductByDateAndPriority(brandId, productId, dateToApplied);
        return new ResponseEntity<>(converter.convert(product), HttpStatus.OK);
    }
}