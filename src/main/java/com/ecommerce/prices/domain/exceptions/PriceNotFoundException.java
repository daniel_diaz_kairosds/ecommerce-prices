
package com.ecommerce.prices.domain.exceptions;

import lombok.extern.log4j.Log4j2;

@Log4j2
public final class PriceNotFoundException extends RuntimeException {
    public PriceNotFoundException(String s) {
        super(s);
    }
}
