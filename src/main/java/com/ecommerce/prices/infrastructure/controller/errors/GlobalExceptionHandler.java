package com.ecommerce.prices.infrastructure.controller.errors;

import com.ecommerce.prices.domain.exceptions.PriceNotFoundException;
import com.ecommerce.prices.infrastructure.controller.dto.ErrorResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
final class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatusCode status, final WebRequest request) {
        return createResponseEntity("The param '" + ex.getParameterName() + "' is required.", HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatusCode status, final WebRequest request) {
        return createResponseEntity("The param '" + ex.getPropertyName() + "' has a mismatched type", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleError(final Exception e, final WebRequest request) {
        return createResponseEntity("An internal error has occurred", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {PriceNotFoundException.class})
    public ResponseEntity<Object> handleContentNotFoundException(Exception e, WebRequest request) {
        return createResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    private static ResponseEntity<Object> createResponseEntity(final String message, final HttpStatus httpStatus) {
        final ErrorResponse errorResponse = new ErrorResponse(httpStatus, message);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }
}
