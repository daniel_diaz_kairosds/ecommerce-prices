package com.ecommerce.prices.domain.model;

import java.util.Objects;

public record ProductId(Integer value) {
    public ProductId {
        if(Objects.isNull(value)){
            throw new IllegalArgumentException("ProductId cannot be null");
        }
    }
}
