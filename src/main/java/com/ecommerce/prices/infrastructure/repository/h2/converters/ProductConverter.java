package com.ecommerce.prices.infrastructure.repository.h2.converters;

import com.ecommerce.prices.domain.model.Product;
import com.ecommerce.prices.infrastructure.repository.h2.entities.Price;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductConverter {
    Product convert(Price input);
}
