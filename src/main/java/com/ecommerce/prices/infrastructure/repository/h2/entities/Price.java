package com.ecommerce.prices.infrastructure.repository.h2.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Price(
        Integer fare,
        Integer productId,
        Integer brandId,
        LocalDateTime startDate,
        LocalDateTime endDate,
        BigDecimal price,
        String currency,
        Integer priority
) {
}
